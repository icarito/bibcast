# Bibcast

Power your voice.

Bibcast is a static site generator for voice casting or publishing audio.

It was designed for the simple need to share and host static recordings.

It is very customizable and supports vue + python components.

## How to use

You can fork this repository and replace the files in `static/`.

Live development can be done running the `bibcast.py` Python application.

First you need to install dependencies:

```
pip3 install -r requirements.txt
cd static
yarn  # or npm -i
```

You can render the static content with `render.py`. For static deployment you will also need to compile assets:

```
yarn run build  # or npm run build
```

See .gitlab-ci.yml for an example for Gitlab CI.

## Features

* Picks up OGG and MP3 files
* Very simplistic design

## License

AGPL v3
